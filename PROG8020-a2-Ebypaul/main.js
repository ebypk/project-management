function checkout() {
    //feching all input fields values
    var fullName = document.getElementById('fullName').value;
    var emailID = document.getElementById('emailID').value;
    var creditCardNum = document.getElementById('creditCardNum').value;
    var ccMonth = document.getElementById('ccMonth').value;
    var ccYear = document.getElementById('ccYear').value;
    var jTShirt = document.getElementById('jTShirt').value;
    var pHat = document.getElementById('pHat').value;
    var jHairCream = document.getElementById('jHairCream').value;
    var jDeodorand = document.getElementById('jDeodorand').value;
    var mWallet = document.getElementById('mWallet').value;
    var UserAddress = document.getElementById('addressArea').value;
    var mobileNumer = document.getElementById('mobileNumber').value;
    var errors = '';
    // Month's text case changing to uppercase
    var monthUpper = ccMonth.toUpperCase();
    // name validation
    var nameRegx = /^[A-Za-z\s]+$/;
    // email validation regex
    var emailRegx = /^[A-Za-z0-9+_.-]+@[A-Za-z0-9+_.-]+$/;
    // credit card validation
    var ccRegx = /^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$/;
    // number validation regex
    var numRegx = /^[0-9]+$/;
    // mobile number validation regex
    var phoneRegx = /^(\d{10}|\d{12})$/;
    // month validation regex
    var monthRegx = /^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)$/;
    var yearRegx = /^(19|20)\d{2}$/; // checks the year is between 2019 to 2099
    // Checking empty submission
    if ((mWallet == '' && jDeodorand == '' && jTShirt == '' && jHairCream == '' && pHat == '') || (mWallet == 0 && jDeodorand == 0 && jTShirt == 0 && jHairCream == 0 && pHat == 0)) {
        errors += "Please fill the quantity to proceed<br>";
    }
    // validating quantity fields
    if (jTShirt != '' && !numRegx.test(jTShirt)) {
        errors += "Please enter valid quantity<br>";
    }
    if (pHat != '' && !numRegx.test(pHat)) {
        errors += "Please enter valid quantity<br>";
    }
    if (jHairCream != '' && !numRegx.test(jHairCream)) {
        errors += "Please enter valid quantity<br>";
    }
    if (jDeodorand != '' && !numRegx.test(jDeodorand)) {
        errors += "Please enter valid quantity<br>";
    }
    if (mWallet != '' && !numRegx.test(mWallet)) {
        errors += "Please enter valid quantity<br>";
    }
    // all fields are validated using regular expression and showing error message
    if (!nameRegx.test(fullName)) {
        errors += "Please enter valid name<br>";
    }
    if (!ccRegx.test(creditCardNum)) {
        errors += "Please provide a valid Credit Card Number<br>";
    }
    if (!monthRegx.test(monthUpper)) {
        errors += "Please provide a vaid month in format MMM<br>";
    }
    if (!yearRegx.test(ccYear)) {
        errors += "Please provide a vaid year in format yyyy<br>";
    }
    if (!emailRegx.test(emailID)) {
        errors += "Please provide a valid email address <br>";
    }
    if (!phoneRegx.test(mobileNumer)) {
        errors += "Please provide a valid phone number <br>";
    }
    if (errors != '') {
        // if the errors are not empty the notification will print in messages div
        document.getElementById('messages').innerHTML = errors;
        // an error class is added for styling
        document.getElementById('messages').classList.add("error");
        document.getElementById('receipt').innerHTML = '';
    }
    else {
        // receipt printing by feting product and user details and calculating the total
        document.getElementById('messages').innerHTML = '';
        document.getElementById('messages').classList.remove("error");
        var receiptData = '<p>Thank you for your purchase</p>';
        // slicing creditcard number to get the final four digit
        var sliceCc = creditCardNum.slice(creditCardNum.length - 4);
        // checking wether user input any null value
        if (isNaN(jTShirt) || jTShirt == null || jTShirt == '') {
            jTShirt = 0;
        }
        if (isNaN(pHat) || pHat == null || pHat == '') {
            pHat = 0;
        }
        if (isNaN(jHairCream) || jHairCream == null || jHairCream == '') {
            jHairCream = 0;
        }
        if (isNaN(jDeodorand) || jDeodorand == null || jDeodorand == '') {
            jDeodorand = 0;
        }
        if (isNaN(mWallet) || mWallet == null || mWallet == '') {
            mWallet = 0;
        }
        // calculating product prices
        var jTShirtPrice = parseInt(jTShirt) * 15;
        var pHatPrice = parseInt(pHat) * 10;
        var jHairCreamPrice = parseInt(jHairCream) * 20;
        var jDeodorandPrice = parseInt(jDeodorand) * 15;
        var mWalletPrice = parseInt(mWallet) * 30;
        var totalPrice = jTShirtPrice + pHatPrice + jHairCreamPrice + jDeodorandPrice + mWalletPrice;
        // calculating donation amount
        var careAmt = totalPrice * 10 / 100;
        if (careAmt < 10) {
            var fixedDon = 10;
            var finalPrice = totalPrice + fixedDon;
        }
        else {
            var fixedDon = careAmt;
            var finalPrice = totalPrice + careAmt;
        }
        // making a form structure here
        receiptData += `
        <table id="userDetails" class="table">
            <tr>
                <td class="receiptHead" colspan="2">H4H Garage Sale</td>
            </tr>
            <tr>
                <td class="receiptSubHead" colspan="2">Receipt</td>
            </tr>
            <tr>
                <td>Name</td>
                <td id="rName"> ${fullName} </td>
            </tr>
            <tr>
                <td>Email</td>
                <td id="rEmail"> ${emailID} </td>
            </tr>
            <tr>
                <td>Mobile Numer</td>
                <td id="mNumer"> ${mobileNumer} </td>
            </tr>
            <tr>
                <td>Address</td>
                <td id="mNumer"> ${UserAddress} </td>
            </tr>
            <tr>
                <td>Credit Card</td>
                <td id="rCc"> XXXX-XXXX-XXXX-${sliceCc} </td>
            </tr>
        </table>
        <table class="table">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Gross Price</th>
                </tr>
            </thead>
            <tbody>
                <tr id="tableT" ${jTShirt == 0 ? 'class="hide"' : ''}>
                    <td>J Brand XL Sized T-Shirt</td>
                    <td id="">${jTShirt}</td>
                    <td>$15</td>
                <td class="pushRight">$${jTShirtPrice.toFixed(2)}</td>
                </tr>
                <tr id="tableHat" ${pHat == 0 ? 'class="hide"' : ''}>
                    <td>Premium Hat</td>
                    <td>${pHat}</td>
                    <td>$10</td>
                    <td class="pushRight">$${pHatPrice.toFixed(2)}</td>
                </tr>
                <tr id="tableHair" ${jHairCream == 0 ? 'class="hide"' : ''}>
                    <td>J Brand Hair Cream 200gm</td>
                    <td>${jHairCream}</td>
                    <td>$20</td>
                    <td class="pushRight">$${jHairCreamPrice.toFixed(2)}</td>
                </tr>
                    <tr id="tabledeo" ${jDeodorand == 0 ? 'class="hide"' : ''}>
                    <td>J Brand Deodorand 500gm</td>
                    <td>${jDeodorand}</td>
                    <td>$15</td>
                    <td class="pushRight">$${jDeodorandPrice.toFixed(2)}</td>
                </tr>
                <tr id="tableWallet" ${mWallet == 0 ? 'class="hide"' : ''}>
                    <td>J Brand Men's Wallet</td>
                    <td>${mWallet}</td>
                    <td>$30</td>
                    <td class="pushRight">$${mWalletPrice.toFixed(2)}</td>
                </tr>
                <tr>
                    <td colspan="3">Donation</td>
                    <td class="pushRight">$${fixedDon.toFixed(2)}</td>
                </tr>
                <tr>
                    <td colspan="3">Total</td>
                    <td class="pushRight">$${finalPrice.toFixed(2)}</td>
                </tr>
            </tbody>
        </table> 
        `;
        // printing receipt in receipt div
        document.getElementById('receipt').innerHTML = receiptData;
    }
    return false;
}