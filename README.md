Steps to build and run the application.

1. Install Visual Studio Code
2. Install node.js
3. Clone the application repo to local
4. Navigate to project directory
5. Run npm install to downlaod missing libraries
6. Run node index.js to run the application
7. Open the browser and type the address localhost:8080 to run the application

I opted for MIT license since it has very less constraints. Anyone who accesses the repository have full freedom to make any changes on the code unless it affects the publisher. This quality of the license allows it to be both business-friendly and open-source friendly, while still making it possible to be monetized
